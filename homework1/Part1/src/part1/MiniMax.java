/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

import java.util.Arrays;

/**
 *
 * @author KiS
 */
public class MiniMax {
   private int[][] input;
   

    public MiniMax(int[][] input) {
        this.input = input;
    }

    private int[] minJ(){
       int[] minJ = new int[input.length];
       for(int i = 0; i<input.length; i++){
           minJ[i] = input[0][0];
           for(int j = 0; j<input[i].length; j++){
               if(input[i][j]< minJ[i]){
                   minJ[i] = input[i][j];
                }
           }
       }
       return minJ;
    }
   
    public void resultMiniMax(){        
        int[] minJ = minJ();
        Arrays.sort(minJ);
        int max = minJ[minJ.length-1];
       System.out.println("MiniMax: " + max);
    }
    
    
}
