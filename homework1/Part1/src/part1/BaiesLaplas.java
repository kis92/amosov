/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

import java.util.Arrays;

/**
 *
 * @author KiS
 */
public class BaiesLaplas {
    private int[][] input;
    private double[] c;

    public BaiesLaplas(int[][] input, double[] c) {
        this.input = input;
        this.c = c;
    }
    
    public void result(){
        double temp[][] = new double[input.length][input[0].length];
        for(int i = 0; i<input.length; i++){
            for(int j=0; j<input[0].length; j++){
                temp[i][j] = input[i][j] * c[i];
            }
        }
        
        double[] maxI = new double[input.length];
        double max = temp[0][0];
        for(int i = 0; i<input.length; i++){
            max = temp[i][0];
            for(int j=0; j<input[0].length; j++){
                if(temp[i][j]>max){
                    max = temp[i][j];
                }
            }
            maxI[i] = max;
        }
        
        Arrays.sort(maxI);
        System.out.println("BL: " + maxI[0]);
    }
}
