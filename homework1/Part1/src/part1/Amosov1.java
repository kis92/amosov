/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

import java.util.Scanner;

/**
 * main class
 *
 * @author KiS
 */
public class Amosov1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[][] matrix = {
            {15, 10, 0, -6, 17},
            {3, 14, 8, 9, 2},
            {1, 5, 14, 20, -3},
            {7, 19, 10, 2, 0}
        };

        int[][] matrixBL = {
            {2400000, 2400000, 2400000, 2400000, 2400000},
            {1900000, 3600000, 3600000, 3600000, 3600000},
            {1400000, 3100000, 4800000, 4800000, 4800000},
            {900000, 2600000, 4300000, 6000000, 6000000},
            {400000, 2100000, 3800000, 5500000, 7200000}
        };

        System.out.println("Minimax = 1");
        System.out.println("Savidg = 2");
        System.out.println("HW = 3");
        System.out.println("BL = 4");
        System.out.println("ALL = 5");
        System.out.println("input num: ");
        Scanner in = new Scanner(System.in);
        int select = in.nextInt();
        MiniMax minimax = new MiniMax(matrix);
        Savidg savidg = new Savidg(matrix);
        Hurbuc hurbuc;
        double[] matrixC = {0.2, 0.15, 0.25, 0.2, 0.2};
        BaiesLaplas bL = new BaiesLaplas(matrixBL, matrixC);
        switch (select) {
            case 1: {
                minimax.resultMiniMax();
                break;
            }
            case 2: {
                savidg.result();
                break;
            }
            case 3: {
                double cr = in.nextDouble();//0.27
                hurbuc = new Hurbuc(matrix, cr);
                hurbuc.result();
                break;
            }
            case 4: {
                bL.result();
                break;
            }
            case 5: {
                System.out.println("ALL:");
                minimax.resultMiniMax();
                savidg.result();
                double cr = 0.27;
                hurbuc = new Hurbuc(matrix, cr);
                hurbuc.result();
                bL.result();
                break;
            }
        }
    }
}
