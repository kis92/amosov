/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

/**
 *
 * @author KiS
 */
public class Utils {
    int[] maxJ(int temp[][]) {
        int tempMaxJ[] = new int[temp[0].length];
        int tempMAX = temp[0][0];
        for (int i = 0; i < temp[0].length; i++) {
            tempMAX = temp[0][i];
            for (int j = 0; j < temp.length; j++) {

                if (temp[j][i] > tempMAX) {
                    tempMAX = temp[j][i];
                }
                tempMaxJ[i] = tempMAX;
            }
        }
        return tempMaxJ;
    }
    
    int[] minJ(int temp[][]) {
        int tempMinJ[] = new int[temp[0].length];
        int tempMIN = temp[0][0];
        for (int i = 0; i < temp[0].length; i++) {
            tempMIN = temp[0][i];
            for (int j = 0; j < temp.length; j++) {
                if (temp[j][i] < tempMIN) {
                    tempMIN = temp[j][i];
                }
                tempMinJ[i] = tempMIN;
            }
        }
        return tempMinJ;
    }
    
    
    public int[] maxImaxJ(int [][] input) {
        int maxMax = input[0][0];
        int maxI[] = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            maxMax = input[i][0];
            for (int j = 0; j < input[i].length; j++) {
                if (input[i][j] > maxMax) {
                    maxMax = input[i][j];
                    maxI[i] = maxMax;
                }
            }

        }
        return maxI;
    }
    
}
