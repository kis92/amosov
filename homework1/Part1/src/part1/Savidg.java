/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

import java.util.Arrays;

/**
 *
 * @author KiS
 */
public class Savidg {

    private int[][] input;

    public Savidg(int[][] input) {
        this.input = input;
    }

    public void result() {
        Utils utils = new Utils();
        int[] maximum = utils.maxImaxJ(input);
        int temp[][] = new int[input.length][input[0].length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                temp[i][j] = maximum[i] - input[i][j];
            }
        }

        int tempMaxJ[] = new int[temp[0].length];
        int tempMAX;

        for (int i = 0; i < temp[0].length; i++) {
            tempMAX = temp[0][i];
            for (int j = 0; j < temp.length; j++) {
                if (temp[j][i] > tempMAX) {
                    tempMAX = temp[j][i];
                }
                tempMaxJ[i] = tempMAX;
            }
        }

        Arrays.sort(tempMaxJ);
        int mini = tempMaxJ[0];
        System.out.println("Savidg: " + mini);
    }

}
