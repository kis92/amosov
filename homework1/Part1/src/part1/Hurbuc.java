/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

import java.util.Arrays;

/**
 *
 * @author KiS
 */
public class Hurbuc {

    private int[][] input;
    private double c;

    public Hurbuc(int[][] input, double c) {
        this.input = input;
        this.c = c;
    }

    public void result() {
        Utils utils = new Utils();
        int[] MINJ = utils.minJ(input);
        double[] tMinJ = new double[MINJ.length];
        for (int i = 0; i < MINJ.length; i++) {
            tMinJ[i] = MINJ[i] * c;
        }

        int[] MAXJ = utils.maxJ(input);
        double[] tMaxJ = new double[MAXJ.length];
        for (int i = 0; i < MAXJ.length; i++) {
            tMaxJ[i] = MAXJ[i] * (1 - c);
        }

        double[] Summ = new double[MAXJ.length];
        for (int i = 0; i < MAXJ.length; i++) {
            Summ[i] = tMaxJ[i] + tMinJ[i];
        }

        Arrays.sort(Summ);
        double hw = Summ[Summ.length - 1];
        System.out.println("HW: " + hw);
    }
}
